PROTOC_MAIN = ./sgpnotifproto.proto
PROTOC_EMAIL = ./email/email.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
GIT_ALL = git add . && git commit -m "--" && git push origin main

git:
	@echo "git all"
	$(GIT_ALL)

main:
	@echo "create sgp main proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_MAIN)
	@echo "create sgp main proto..."

email:
	@echo "create sgp email proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_EMAIL)
	@echo "create sgp email proto..."

all: email main

.PHONY: main all git email 
